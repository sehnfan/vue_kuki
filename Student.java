import java.util.*;
import java.util.Scanner;
//我爱学习
public class SchoolSystem_1 implements ISignUp{
	Integer big;
	Integer medium;
	Integer small;
	ArrayList<Integer> plan ;
	ArrayList<Boolean> plan_1 ;
	public SchoolSystem_1(Integer big,Integer medium,Integer small) {
		this.big=big;
		this.medium=medium;
		this.small=small;
		
	}
	public static void main(String[] args) throws Exception {
		  
	      IParams params = ISignUp.parse();//SchoolSystem.parse();
	      SchoolSystem_1 sc = new SchoolSystem_1 (params.getBig(),params.getMedium(),params.getSmall());
	      ArrayList<Integer> plan_1 =params.getPlanSignUp();
	      ArrayList<Boolean> plan_2 =new ArrayList();
	      
	      for (int i = 0; i < plan_1.size(); i++) {
	         plan_2.add(sc.addStudent(plan_1.get(i)));
	      }
	      sc.print(plan_2);
		}
//判断学生类型
	public boolean addStudent(int stuType) {
		 if(stuType==1) {
			if(big>0) {
			return true;
			}
			big--;
		}
		else if(stuType==2) {
			if(medium>0) {
				
			return true;
			}
			medium--;
		}
		else if(stuType==3) {
			if(small>0) {
				
			return true;
			}
			small--;
		}
		return false;
			
	}
	public void print(ArrayList<Boolean> plan_2) {
		
			System.out.println(plan_2);
		
	}
	@Override
	public void print() {
		// TODO Auto-generated method stub
		
	}
}
	
	
	
	interface ISignUp{
		  // 打印输出结果
		  public void print();
		  // 检查是否有 stuType对应的班级名额
		  // 如果没有剩余名额，请返回 false ，否则学生将报名进入该班级并返回 true
		  public boolean addStudent (int stuType);
		  // 解析命令行输入的参数（格式），如文档描述
		  public static IParams parse() throws Exception{
			  //班级剩余人数
			Scanner scn=new Scanner(System.in);
			Integer big=scn.nextInt();
			Integer medium=scn.nextInt();
			Integer small=scn.nextInt();
			ArrayList<Integer> plan =new ArrayList<>();
			//报名顺序输入
			for(int i=0;i<4;i++) {
				plan.add(scn.nextInt());
			}
			return new IParams() {
				public int getBig() {
					return big;
				}
				public int getMedium() {
					return medium;
				}
				public int getSmall() {
					return small;
				}
				public ArrayList<Integer> getPlanSignUp() {
					return plan;
				}
			};
		  }
		}
	interface IParams {
	    // 获取大班名额
	    public int getBig();
	    // 获取中班名额
	    public int getMedium();
	    // 获取小班名额
	    public int getSmall();
	    // 获取入学序列，例如 [1 2 2 3] 表示依次报名入学一名大班、中班、中班、小班学生
	    public ArrayList<Integer> getPlanSignUp ();
	}